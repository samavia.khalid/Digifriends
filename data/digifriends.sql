-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2016 at 10:35 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `digifriends`
--
CREATE DATABASE IF NOT EXISTS `digifriends` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `digifriends`;

-- --------------------------------------------------------

--
-- Table structure for table `digitfriends`
--

DROP TABLE IF EXISTS `digitfriends`;
CREATE TABLE IF NOT EXISTS `digitfriends` (
  `digit` bigint(20) NOT NULL,
  `digitFriend` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `digitfriends`
--

INSERT INTO `digitfriends` (`digit`, `digitFriend`) VALUES
(6000, 391),
(7000, 426),
(7000, 426),
(70002, 1331),
(700, 136),
(700, 271),
(700, 541),
(700, 676),
(600, 126),
(600, 251),
(600, 501),
(45, 41);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(10) NOT NULL AUTO_INCREMENT,
  `userName` varchar(30) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `userdigit`
--

DROP TABLE IF EXISTS `userdigit`;
CREATE TABLE IF NOT EXISTS `userdigit` (
  `userId` int(11) NOT NULL,
  `userDigit` bigint(20) NOT NULL,
  `createTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdigit`
--

INSERT INTO `userdigit` (`userId`, `userDigit`, `createTimestamp`) VALUES
(1, 3, '2016-03-28 17:44:00'),
(1, 700, '2016-03-28 18:16:13'),
(1, 600, '2016-03-28 18:33:16'),
(1, 45, '2016-03-28 20:00:40');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
