<?php

namespace Application\Service;

use Application\Model\DigitFriendRepository;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class DigitFriendFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var DigitFriendRepository $repository */
        $repository = $serviceLocator->get('Application\Model\DigitFriends');
        $service = new DigitFriend();
        $service->setRepository($repository);
        return $service;
    }
}