<?php

namespace Application\Service;

use Application\Model\UserDigitRepository;
use Application\Service\DigitFriend;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserDigitFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var UserDigitRepository $repository */
        $repository = $serviceLocator->get('Application\Model\UserDigit');
        /** @var DigitFriend $digitFriendService */
        $digitFriendService = $serviceLocator->get('Application\Service\DigitFriend');

        $service = new UserDigit($repository, $digitFriendService);

        return $service;
    }
}