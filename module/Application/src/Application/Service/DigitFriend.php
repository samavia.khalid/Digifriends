<?php
namespace Application\Service;

use Application\Model\DigitFriendRepository;

class DigitFriend
{
    /** @var  DigitFriendRepository */
    private $repository;

    /**
     * @return DigitFriendRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param DigitFriendRepository $repository
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function computeFriendsOfDigit($digitX)
    {
        for ($i = 1; $i <= $digitX; $i = $i + 1) {

            $DF = -1;
            if ($i % 3 == 0) {
                $DF = $i + ($i * $digitX);
            } elseif ($i % 5 == 0) {
                $DF = $i + ($i * round(sqrt($digitX))) + 1;
            } elseif ($i % 5 == 0 && $i % 3 == 0) {
                $DF = $i + $i * 3 * 5 * ROUND(SQRT($digitX)) + 2;
            }
            if ($DF > -1 && ($DF <= $digitX)) {
                $this->getRepository()->presistDigitFriend(
                    [
                        'digit' => $digitX,
                        'digitFriend' => $DF
                    ]
                );
            }
        }
    }
}