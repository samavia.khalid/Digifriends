<?php
namespace Application\Service;

use Application\Model\UserDigit as UserDigitRequest;
use Application\Model\UserDigitRepository;

class UserDigit
{
    /** @var  UserDigitRepository */
    private $userDigitRepository;
    private $digitFriendService;

    public function __construct(UserDigitRepository $userDigitRepository, DigitFriend $digitFriend)
    {
        $this->userDigitRepository = $userDigitRepository;
        $this->digitFriendService = $digitFriend;

    }

    public function processUserDigit($data)
    {
        $UserDigitModel = new UserDigitRequest($data);
        $this->userDigitRepository->insert($UserDigitModel->getArrayCopy());
        $this->digitFriendService->computeFriendsOfDigit(
            $UserDigitModel->getUserDigit()
        );
    }

    public function getUserDigitWithFriends($userId)
    {
        $results = $this->userDigitRepository->findUserDigitFriends($userId);
        $dataSet = [];
        $key=0;
        while ($row = $results->fetch()) {
            if($key!==$row['digit'])
            {
                $key =  $row['digit'];
            }

            $dataSet[$key][] = $row['digitFriend'];
        }

        return $dataSet;
    }
}