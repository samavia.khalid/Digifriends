<?php

namespace Application\Forms;


use Zend\Form\Form;

class UserDigiForm extends Form {

    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('UserDigiForm');


        $this->add(array(
            'name' => 'userDigit',
            'type' => 'Text',
            'options' => array(
                'label' => 'Digit',
            ),
        ));
        $this->add(array(
            'name' => 'Submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Make Friends',
                'id' => 'submitbutton',
            ),
        ));
    }

}