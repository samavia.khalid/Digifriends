<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\PhpEnvironment\Request;
use Zend\View\Model\ViewModel;
use Application\Forms\UserDigiForm;
use Application\Service\UserDigit;

class UserController extends AbstractActionController
{

    /** @var UserDigit $userDigitService */
    private $userDigitService;

    public function indexAction()
    {
        return new ViewModel(['form' => new UserDigiForm()]);
    }

    public function makeUserDigitFriendAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $params = array_merge(
            $request->getQuery()->toArray(),
            $request->getPost()->toArray()
        );
        try {
            $this->getUserDigitService()->processUserDigit($params);
            return $this->redirect()->toRoute(null, array(
                'module'     => 'application',
                'controller' => 'user',
                'action'     => 'current'
            ));
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function currentAction()
    {

        $userDigit =  $this->getUserDigitService()->getUserDigitWithFriends(1);

        return new ViewModel(['ResultSet' => $userDigit]);
    }

    /**
     * @return UserDigit
     */
    public function getUserDigitService()
    {
        return $this->userDigitService;
    }

    /**
     * @param UserDigit $userDigitService
     */
    public function setUserDigitService($userDigitService)
    {
        $this->userDigitService = $userDigitService;
    }
}