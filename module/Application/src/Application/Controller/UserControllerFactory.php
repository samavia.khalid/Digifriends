<?php
/**
 * Created by PhpStorm.
 * User: madiha
 * Date: 28/03/2016
 * Time: 13:35
 */

namespace Application\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Service\UserDigit;

class UserControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sm = $serviceLocator->getServiceLocator();
        /** @var  UserDigit $userDigitService */
        $userDigitService = $sm->get('UserDigitService');
        $controller = new UserController();
        $controller->setUserDigitService($userDigitService);
        return $controller;
    }
}