<?php
namespace Application\Model;

class DigitFriendRepository extends AbstractRepository
{

    public function presistDigitFriend($param)
    {
        if ($this->find($param)->count() === 0) {
            $this->insert($param);
        }
    }
}