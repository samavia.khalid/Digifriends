<?php
namespace Application\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class AbstractRepository {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    public function insert($data)
    {
           try{
               return  $this->tableGateway->insert($data);
           }
           catch(\Exception $e )
           {
               throw($e);
        }
    }
    public function find($whereParams)
    {
        try{
         return   $this->tableGateway->select($whereParams);
        }
        catch(\Exception $e )
        {
            throw($e);
        }
    }
    public function findUserDigitFriends($userId)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = 'SELECT
        userId,digit,digitFriend,createTimeStamp
        FROM digitfriends,userdigit
        WHERE userDigit = digit and userId='.(int)$userId.'
        ORDER BY createTimeStamp DESC';
        $resultSet = $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql);
        return $resultSet->getResource();
    }
}