<?php
namespace Application\Model;

class DigitFriends {
    public $xUser;
    public $xNumber;
    public $xFriends;

    public function exchangeArray($data)
    {
        $this->xUser     = (!empty($data['xUser'])) ? $data['xUser'] : null;
        $this->xNumber = (!empty($data['xNumber'])) ? $data['xNumber'] : null;
        $this->xFriends = (!empty($data['xFriends'])) ? $data['xFriends'] : null;
    }
}