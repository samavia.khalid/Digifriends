<?php
namespace Application\Model;

class UserDigit
{
    public $userId;
    public $userDigit;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getUserDigit()
    {
        return $this->userDigit;
    }


    public function __construct($data = null)
    {
          $this->exchangeArray($data);
    }
    public function getArrayCopy()
    {
        return [
            'userId' => $this->userId,
            'userDigit' => $this->userDigit
        ];
    }
    public function exchangeArray($data)
    {
        $this->userId = (!empty($data['userId'])) ? $data['userId'] : null;
        $this->userDigit = (!empty($data['userDigit'])) ? $data['userDigit'] : null;

    }
}