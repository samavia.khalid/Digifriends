<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Model\DigitFriendRepository;
use Application\Model\UserDigit;
use Application\Model\UserDigitRepository;
use Application\Service\DigitFriendFactory;
use Application\Service\UserDigitFactory;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Model\DigitFriends' =>  function($sm) {

                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $tableGateway = new TableGateway('DigitFriends', $dbAdapter, null);
                    $table = new DigitFriendRepository($tableGateway);
                    return $table;
                },
                'Application\Model\UserDigit' =>  function($sm) {
                    $tableGateway = $sm->get('UserDigitRepository');
                    $table = new UserDigitRepository($tableGateway);
                    return $table;
                },
                'UserDigitRepository' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserDigit());
                    return new TableGateway('UserDigit', $dbAdapter, null, $resultSetPrototype);
                },
                'UserDigitService' =>  function($ServiceManager){
                    $factory = new UserDigitFactory();
                    return $factory->createService($ServiceManager);
                },
                'Application\Service\DigitFriend' =>  function($ServiceManager){
                    $factory = new DigitFriendFactory();
                    return $factory->createService($ServiceManager);
                }
            ),
        );
    }

}
