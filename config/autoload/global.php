<?php
return array(
    'db' => array(
        'username' => 'root',
        'password' => '',
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=digifriends;host=localhost',
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
            => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);